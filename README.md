psudo

Pseudocode	for	backtracking	search	with	forward	checking.	
A	=	assignment
D	=	domain	of	variables
U	=	unassigned	variables
BT+FC(A,	U,	D)
if	A	is	complete	then	
return A	
end	if	
Remove	a	variable	X	from	U	
for	all values	x	∈ D(X)	do	
if	X	=	x	is	consistent	with	A	according	to	the	constraints	then
Add	X	=	x	to	A
Dʹ	←	D	(Save	the	current	domains)
for	all Y	∈ U	(i.e.,	Y	an	unassigned	variable),	Y	−−−X	(i.e.,	Y	a	neighbor	of	X	in	the	
constrained	graph)	do	
Remove	values	for	Y	from	Dʹ(Y	)	that	are	inconsistent	with	A	
end	for	
if			for all Y	∈U,Y	−−−X, Dʹ(Y) not empty then	
result	←	BT+FC(A,	U,	Dʹ)
													if result	̸=	failure	then
return result	
																																							end	if	
end	if	
Remove	X	=	x	from	A	
														end	if	
end	for
return failure	
