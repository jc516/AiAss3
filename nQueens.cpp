// n queens backtracking algorithm
#include <iostream>
#include <cstdlib>
#include <cassert>
bool oneQueen(bool **table, int size);
using namespace std;

bool allocate(bool **table, int nrQueens, int nextCol);
bool checkAssignment(bool **table, int nrQueens, int indRow, int indCol);
void dispTable(bool **table, int size);

int main()
{
 	int nq; // number of queens
 	bool **table;

 	// initialize nq
 	cout << "How many queens " << endl;
	cin >> nq;

	// allocate memory for table
	table = new bool *[nq];
	for (int i = 0; i < nq ; i++)
		table[i] = new bool[nq];

	// initialize memory to 0 (false)
	for (int i = 0; i < nq; i++)
		for (int j = 0; j < nq; j++)
			table[i][j] = 0;

	bool res = allocate(table, nq,0);

	if (res) // valid position found
	  // display result
	  dispTable(table, nq);
   else
	   cout << "No valid arrangement was found " << endl;

   cout << "END " << endl;
   char c;
   cin >> c;
}//end main

bool allocate(bool **table, int nrQueens, int nextCol)
{
    assert (oneQueen(table,nrQueens)); // check that there is only one queen per each col
 	if (nextCol == nrQueens)
 	   return 1; // all queens are fine
    else
    {
	 	bool success = false;
	 	int indRow = 0;
	 	while (!success && indRow < nrQueens)
	 	{
	         if (indRow > 0)
	           table[indRow-1][nextCol] = 0; // reverse the previous position
	         table[indRow][nextCol] = 1; // put the queen on nextCol on indRow
	         if (checkAssignment(table, nrQueens, indRow, nextCol)) // no attacks
	             success  = allocate(table, nrQueens, nextCol+1);
	             // backtrack the position of the queen
	         indRow++;
	    }
	    if (!success)
	      table[indRow-1][nextCol] = 0;
        return success;
	}
}//end allocate

// precondition = all queens placed on columns 0 to indCol-1 do not attack each other
bool checkAssignment(bool **table, int nrQueens, int indRow, int indCol)
{
 	 cout << "Inside checkAssignment " << endl;
 	 cout << indRow << ' '  << indCol << endl;
 	 dispTable(table, nrQueens);
 	 int nrAttacks = 0;
 	 for (int c = 0; c < indCol; c++) // check each queen on previous columns
 	 {
	  	 // find queen on c column
	  	 int r = 0;
	  	 while(table[r][c]== 0 && r < nrQueens)
           r++;
	 	//check row or column index
	 	// cout << "r=" << r << "c=" << c << endl;
	 	if (r == indRow || c == indCol)
	 	   nrAttacks++;
        else if ( (r-c == indRow-indCol) || (r+c == indRow+indCol) )
           nrAttacks++;
        //cout << "nrAttacks = "<< nrAttacks << endl;
	 }
	 cout << "Output checkAttacks " << nrAttacks << endl;
	 return (nrAttacks == 0);

}

void dispTable(bool **table, int size)
{
 	 for (int i = 0; i < size; i++)
     {
	  	 for (int j =0; j < size; j++)
	  	   cout << table[i][j] << ' ';
 	     cout << endl;
    }
}

bool oneQueen(bool **table, int size)
{
 	 bool one = true;
 	 int c = 0;
 	 while (one && c < size)
 	 {
	  	 int sumQ = 0;
	  	 for(int r = 0; r < size; r++)
	  	     sumQ += table[r][c];
         one = (sumQ <= 1);
         c++;
	 }
	 return one;
}
